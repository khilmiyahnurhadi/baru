﻿using loan.Dtos.User;
using loan.Models.User;
using loan.Repositories;
using loan.WorkFlows.Listeners;
using MassTransit;
using QSI.Common.Helper;
using QSI.Security.Api.Common.Command;
using QSI.Security.Api.Common.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.WorkFlows.Impl
{
    public class UserAccountRegistrationWorkflowImpl : IUserAccountRegistrationWorkflow
    {
        private readonly IUserProfileProDao userProfileProDao;
        private readonly IRequestClient<CreateNewUserCommand> requestClient;
        public IUserAccountRegistrationWorkflowListener workflowListener { private get; set; }
        public IdGenerator usernameGenerator { private get; set; }

        public UserAccountRegistrationWorkflowImpl(IUserProfileProDao userProfileProDao, IRequestClient<CreateNewUserCommand> requestClient)
        {
            this.userProfileProDao = userProfileProDao;
            this.requestClient = requestClient;
        }

        public async Task<UserAccountRegisteredDto> RegisterUserAccount(UserBase userAccount, string username = null)
        {
            UserProfilePro userProfilePro = null;
            string otp = string.Empty;

            try
            {
                //Place anything you want to execute / process before registration
                if (workflowListener != null)
                    userAccount = workflowListener.BeforeRegistration(userAccount);

                userProfilePro = userProfileProDao.GetByUsernameCode(userAccount.Username, "UserBase");

                //New User
                if (userProfilePro == null)
                {
                    if (username == null)
                        username = usernameGenerator.Generate(userAccount);

                    var command = new CreateNewUserCommand()
                    {
                        Username = username,
                        Password = "password"
                    };

                    if (workflowListener != null)
                        command = workflowListener.OnCreateUser(command);

                    //Send request to MQ via MassTransit to create user in sec_user
                    var userCreatedResponse = await requestClient.GetResponse<UserCreatedResponse>(command);

                    userProfilePro = new UserProfilePro()
                    {
                        UserProfile = userAccount,
                        Username = userCreatedResponse.Message.Username,
                        UserId = userCreatedResponse.Message.Id.ToString(),
                        Status = QSI.Persistence.Model.RecordStatus.Active
                    };
                }
                else
                {
                    username = userAccount.Username;
                }

                if (workflowListener != null)
                    userProfilePro = workflowListener.OnRegisterUser(userProfilePro);

                if (workflowListener != null)
                    otp = workflowListener.AfterUserRegistration(userProfilePro).GetAwaiter().GetResult();

                userProfileProDao.Save(userProfilePro);
            }
            catch (Exception ex)
            {
                if (workflowListener != null)
                    workflowListener.OnError(ex);
                else
                    throw;
            }

            return new UserAccountRegisteredDto() { Username = username, Otp = otp };
        }
    }
}
