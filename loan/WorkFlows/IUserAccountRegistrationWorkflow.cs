﻿using loan.Dtos.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.WorkFlows
{
    public interface IUserAccountRegistrationWorkflow
    {
        Task<UserAccountRegisteredDto> RegisterUserAccount(Models.User.UserBase userAccount, string username = null);
    }
}
