﻿using loan.Models.User;
using QSI.Security.Api.Common.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.WorkFlows.Listeners
{
    public interface IUserAccountRegistrationWorkflowListener
    {
        void OnError(Exception e);
        Models.User.UserBase BeforeRegistration(Models.User.UserBase userProfile);
        CreateNewUserCommand OnCreateUser(CreateNewUserCommand command);
        UserProfilePro OnRegisterUser(UserProfilePro userProfilePro);
        Task<string> AfterUserRegistration(UserProfilePro userProfilePro);
    }
}
