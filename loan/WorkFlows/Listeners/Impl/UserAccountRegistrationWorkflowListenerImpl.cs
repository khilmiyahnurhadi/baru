﻿using loan.Models.User;
using MassTransit;
using QSI.Notification;
using QSI.Notification.Api.Common.Command;
using QSI.Security.Api.Common.Command;
using QSI.Security.Api.Common.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.WorkFlows.Listeners.Impl
{
    public class UserAccountRegistrationWorkflowListenerImpl : IUserAccountRegistrationWorkflowListener
    {
        private readonly IRequestClient<CreateNewOtpCommand> requestNewOtpClient;
        private readonly IRequestClient<VerifyOtpCommand> requestClient;
        private readonly IBusControl busControl;

        public UserAccountRegistrationWorkflowListenerImpl(IRequestClient<CreateNewOtpCommand> requestNewOtpClient, IRequestClient<VerifyOtpCommand> requestClient, IBusControl busControl)
        {
            this.requestNewOtpClient = requestNewOtpClient;
            this.requestClient = requestClient;
            this.busControl = busControl;
        }

        public async Task<string> AfterUserRegistration(UserProfilePro userProfilePro)
        {
            var response = await requestNewOtpClient.GetResponse<OtpCreationResponse>(new CreateNewOtpCommand
            {
                Application = "Registration.User",
                VerificationId = userProfilePro.UserProfile.Username
            });

            string otp = response.Message.Otp;
            await busControl.Publish<SendNotificationCommand>(new
            {
                Id = Guid.NewGuid(),
                Type = NotificationType.Email,
                Title = "[Confidential] Your One Time Password - Ayusoo Cantik",
                Message = $"Hi {userProfilePro.UserProfile.FullName}, <br /><br /> Your OTP is {otp}",
                Recipients = new string[] { userProfilePro.UserProfile.Email }
            });

            return otp;
        }

        public UserBase BeforeRegistration(UserBase userProfile)
        {
            return userProfile;
        }

        public CreateNewUserCommand OnCreateUser(CreateNewUserCommand command)
        {
            return command;
        }

        public void OnError(Exception e)
        {
            throw e;
        }

        public UserProfilePro OnRegisterUser(UserProfilePro userProfile)
        {
            userProfile.Status = QSI.Persistence.Model.RecordStatus.Confirming;
            return userProfile;
        }
    }
}
