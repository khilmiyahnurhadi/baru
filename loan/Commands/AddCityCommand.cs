﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class AddCityCommand
    {
        public string CityName { set; get; }
        public long ProvinceId { set; get; }
    }
}
