﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class AddSubDistrictCommand
    {
        public string SubDistrictName { set; get; }
        public long CityId { set; get; }
    }
}
