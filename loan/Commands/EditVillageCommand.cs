﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class EditVillageCommand
    {
        public long IdVillage { set; get; }
        public string VillageName { set; get; }
        public long PostalCodeId { set; get; }
    }
}
