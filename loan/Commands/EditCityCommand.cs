﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class EditCityCommand
    {
        public long IdCity { set; get; }
        public string CityName { set; get; }
        public long ProvinceId { set; get; }
    }
}
