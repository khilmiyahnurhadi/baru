﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class AddVillageCommand
    {
        public string VillageName { set; get; }
        public long PostalCodeId { set; get; }
    }
}
