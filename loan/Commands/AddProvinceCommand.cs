﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class AddProvinceCommand
    {
        public string ProvinceName { set; get; }
    }
}
