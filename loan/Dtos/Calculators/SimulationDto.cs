﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos.Calculators
{
    public class SimulationDto
    {
        public long Id { set; get; }
        public long PropertyPrices { set; get; }
        public long PTKP { set; get; }
        public long DownPayment { set; get; }
        public decimal LoanInterestCost { set; get; }
        public decimal HomeInsuranceCost { set; get; }
        public long LifeInsuranceCost { set; get; }
        public decimal AdministrativeCost { set; get; }
        public decimal BPHTB { set; get; }
        public decimal NotaryFee { set; get; }
        public long PNPB { set; get; }
        public long StampDuty { set; get; }

        public SimulationDto()
        {

        }

        public SimulationDto(long id, long propertyPrices, long pTKP, long downPayment, decimal loanInterestCost, decimal homeInsuranceCost, long lifeInsuranceCost, decimal administrativeCost, decimal bPHTB, decimal notaryFee, long pNPB, long stampDuty)
        {
            Id = id;
            PropertyPrices = propertyPrices;
            PTKP = pTKP;
            DownPayment = downPayment;
            LoanInterestCost = loanInterestCost;
            HomeInsuranceCost = homeInsuranceCost;
            LifeInsuranceCost = lifeInsuranceCost;
            AdministrativeCost = administrativeCost;
            BPHTB = bPHTB;
            NotaryFee = notaryFee;
            PNPB = pNPB;
            StampDuty = stampDuty;
        }
    }
}
