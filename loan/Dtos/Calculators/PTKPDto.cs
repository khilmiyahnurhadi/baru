﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos.Calculators
{
    public class PTKPDto
    {
        public bool Status { set; get; }
        public bool MateHasIncome { set; get; }
        public Int16 Burden { set; get; }
        public Int64 TotalPTKP { set; get; }

        public PTKPDto()
        {

        }

        public PTKPDto( bool status, bool mateHasIncome, Int16 burden, Int64 totalPTKP)
        {
            Status = status;
            MateHasIncome = mateHasIncome;
            Burden = burden;
            TotalPTKP = totalPTKP;
        }
    }
}
