﻿using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos
{
    public class SubDistrictDto
    {
        public long Id { set; get; }
        public string SubDistrictName { set; get; }
        public long CityId { get; set; }

        public SubDistrictDto()
        { }

        public SubDistrictDto(long id, string subDistrictName, long cityId)
        {
            Id = id;
            SubDistrictName = subDistrictName;
            CityId = cityId;
        }

    }
}
