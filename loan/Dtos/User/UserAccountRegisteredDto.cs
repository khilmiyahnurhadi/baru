﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos.User
{
    public class UserAccountRegisteredDto
    {
        public string Username { get; set; }
        public string Otp { get; set; }
    }
}
