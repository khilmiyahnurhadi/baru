﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos.User
{
    public class UserAccountRegistrationHintDto
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Otp { get; set; }
    }
}
