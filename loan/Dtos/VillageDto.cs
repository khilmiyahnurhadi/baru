﻿using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos
{
    public class VillageDto
    {
        public long Id { set; get; }
        public string VillageName { set; get; }

        public VillageDto()
        { }

        public VillageDto(long id, string villageName)
        {
            Id = id;
            VillageName = villageName;
        }
    }
}
