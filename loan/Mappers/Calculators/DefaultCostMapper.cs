﻿using FluentNHibernate.Mapping;
using loan.Models.Calculators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers.Calculators
{
    public class DefaultCostMapper : ClassMap<DefaultCost>
    {
        public DefaultCostMapper()
        {
            Table("t_defaultcost");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.CostName).Not.Nullable();
            Map(x => x.Cost).Not.Nullable();
        }
    }
}
