﻿using FluentNHibernate.Mapping;
using loan.Models.User;
using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers.User
{
    public class UserProfileProMapper : ClassMap<UserProfilePro>
    {
        public UserProfileProMapper()
        {
            Table("user_profile");

            Id(x => x.Id).GeneratedBy.Native();
            References<UserBase>(x => x.UserProfile).ForeignKey().LazyLoad().Not.Nullable();
            Map(x => x.UserId).Not.Nullable();
            Map(x => x.Username).Not.Nullable();
            Map(x => x.Status).CustomType<RecordStatus>().Not.Nullable();
        }
    }
}
