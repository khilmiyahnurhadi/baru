﻿using FluentNHibernate.Mapping;
using loan.Models.User;
using QSI.ORM.NHibernate.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers.User
{
    public class UserBaseMapper : BaseAuditedEntityMapper<UserBase, long>
    {
        public UserBaseMapper() : base()
        {
            Table("user_base");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Username).Not.Nullable();
            Map(x => x.FullName).Not.Nullable();
            Map(x => x.PhoneNumber).Not.Nullable();
            Map(x => x.Email).Not.Nullable();
        }
    }
}
