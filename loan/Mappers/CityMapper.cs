﻿using FluentNHibernate.Mapping;
using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers
{
    public class CityMapper : ClassMap<City>
    {
        public CityMapper()
        {
            Table("t_city");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.CityName).Not.Nullable();
            References<Province>(x => x.ProvinceId).Column("provinceid_id").Not.Nullable();
            HasMany(x => x.SubDistricts)
                .Inverse()
                .AsBag()
                .LazyLoad();
        }
    }
}
