﻿
using FluentNHibernate.Mapping;
using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers
{
    public class SubDistrictMapper : ClassMap<SubDistrict>
    {
        public SubDistrictMapper()
        {
            Table("t_subdistrict");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.SubDistrictName).Not.Nullable();
            References<City>(x => x.CityId).Not.Nullable();
            HasMany(x => x.PostalCodes)
                .Inverse()
                .AsBag()
                .LazyLoad();
        }
    }
}
