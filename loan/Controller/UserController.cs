﻿using loan.Commands;
using loan.Dtos.User;
using loan.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public IUserAccountService userService;

        public UserController(IUserAccountService userService)
        {
            this.userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public string AddNewUserAccount(RegisterUserAccountCommand command)
        {
            return userService.RegisterNewUserAccount(command);
        }

        [AllowAnonymous]
        [HttpPost("register/user")]
        public async Task<UserAccountRegistrationHintDto> ActivateUserAccount([FromBody] UserAccountRegistratoionDto userAccountRegistration)
        {
            return await userService.RegisterUserAccount(userAccountRegistration.UserAccountName,userAccountRegistration.Username);
        }
    }
}
