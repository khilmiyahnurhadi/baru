﻿using loan.Commands;
using loan.Dtos;
using loan.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        public ILocationService locationService;

        public LocationController(ILocationService locationService)
        {
            this.locationService = locationService;
        }

        [HttpPost]
        [Route("Province")]
        [AllowAnonymous]
        public void AddNewProvince([FromBody] AddProvinceCommand command)
        {
            this.locationService.AddNewProvince(command);
        }

        [HttpPost]
        [Route("City")]
        [AllowAnonymous]
        public void AddNewCity([FromBody] AddCityCommand command)
        {
            this.locationService.AddNewCity(command);
        }

        [HttpPost]
        [Route("SubDistrict")]
        [AllowAnonymous]
        public void AddNewSubDistrict([FromBody] AddSubDistrictCommand command)
        {
            this.locationService.AddNewSubDistrict(command);
        }

        [HttpPost]
        [Route("PostalCode")]
        [AllowAnonymous]
        public void AddNewPostalCode([FromBody] AddPostalCodeCommand command)
        {
            this.locationService.AddNewPostalCode(command);
        }

        [HttpPost]
        [Route("Village")]
        [AllowAnonymous]
        public void AddNewVillage([FromBody] AddVillageCommand command)
        {
            this.locationService.AddNewVillage(command);
        }

        [HttpPut]
        [Route("Province")]
        [AllowAnonymous]
        public void EditProvince([FromBody]EditProvinceCommand command)
        {
            this.locationService.EditProvince(command);
        }

        [HttpPut]
        [Route("City")]
        [AllowAnonymous]
        public void EditCity([FromBody]EditCityCommand command)
        {
            this.locationService.EditCity(command);
        }

        [HttpPut]
        [Route("SubDistrict")]
        [AllowAnonymous]
        public void EditSubDistrict([FromBody]EditSubDistrictCommand command)
        {
            this.locationService.EditSubDistrict(command);
        }

        [HttpPut]
        [Route("PostalCode")]
        [AllowAnonymous]
        public void EditPostalCode([FromBody]EditPostalCodeCommand command)
        {
            this.locationService.EditPostalCode(command);
        }

        [HttpPut]
        [Route("Village")]
        [AllowAnonymous]
        public void EditVillage([FromBody]EditVillageCommand command)
        {
            this.locationService.EditVillage(command);
        }

        [HttpGet]
        [Route("Province")]
        [AllowAnonymous]
        public IList<ProvinceDto> GetAllProvince()
        {
            return locationService.GetAllProvince();
        }

        [HttpGet]
        [Route("City")]
        [AllowAnonymous]
        public IList<CityDto> GetAllCity()
        {
            return locationService.GetAllCity();
        }

        [HttpGet]
        [Route("SubDistrict")]
        [AllowAnonymous]
        public IList<SubDistrictDto> GetAllSubDistrict()
        {
            return locationService.GetAllSubDistrict();
        }

        [HttpGet]
        [Route("PostalCode")]
        [AllowAnonymous]
        public IList<PostalCodeDto> GetAllPostalCode()
        {
            return locationService.GetAllPostalCode();
        }

        [HttpGet]
        [Route("Village")]
        [AllowAnonymous]
        public IList<VillageDto> GetAllVillage()
        {
            return locationService.GetAllVillage();
        }
    }
}
