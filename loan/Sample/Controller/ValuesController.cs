using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using loan.Dto;

namespace loan.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<SampleDto>> Get()
        {            
            return new SampleDto[] {  
				new SampleDto {
					Id = Guid.NewGuid(),
					Name = "John",
					Dob = new DateTime(1986, 10, 9),
					Salary = 100000000,
					GraduateYear = 2017,
					Height = SampleEnum.Tall
				},
				new SampleDto {
					Id = Guid.NewGuid(),
					Name = "Marcel",
					Dob = new DateTime(1986, 2, 25),
					Salary = 2000,
					GraduateYear = 2009,
					Height = SampleEnum.Short
				}
			};
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<SampleDto> Get(Guid id)
        {
            return new SampleDto {
					Id = Guid.NewGuid(),
					Name = "Joseph",
					Dob = new DateTime(1986, 6, 1),
					Salary = 100000,
					GraduateYear = 2000,
					Height = SampleEnum.Tall
				};
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] SampleDto value)
        {
			
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody] SampleDto value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
        }
    }
}
