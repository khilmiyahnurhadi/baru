using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
namespace loan.Dto
{
	public class SampleDto
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public DateTime Dob { get; set; }
		public decimal Salary { get; set; }
		public int GraduateYear { get; set; }
		public SampleEnum Height { get; set; }
	}
}