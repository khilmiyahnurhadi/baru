using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Extras.DynamicProxy;
using AutoMapper;
using loan.Extensions;
using MassTransit.RabbitMqTransport;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using QSI.Automapper.Extension;
using QSI.MassTransit.Boot.Starter.AspNetCore;
using QSI.MassTransit.Boot.Starter.Configurations;
using QSI.MassTransit.Boot.Starter.Extensions;
using QSI.Notification;
using QSI.Notification.Api.AspNetCore.Extension;
using QSI.Notification.Email;
using QSI.Notification.Mobile;
using QSI.Notification.SMS;
using QSI.ORM.Config;
using QSI.ORM.NHibernate.Extension;
using QSI.ORM.NHibernate.Interceptor;
using QSI.Security.Api.AspNetCore.Extension;
using QSI.Security.Api.MassTransit.Client.Extensions;
using QSI.Security.JWT;
using QSI.Security.Login;
using QSI.Security.Service;
using QSI.Service.Http.Middleware;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace loan
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
				.AddYamlFile("configuration.yml", optional: false, reloadOnChange: true)
				.AddYamlFile($"configuration.{env.EnvironmentName}.yml", optional: true, reloadOnChange: true);
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }
		
		public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
			var builder = new ContainerBuilder();
			services.AddSingleton(Configuration);
			
			var jwtsetting = new JwtConfiguration();
            Configuration.Bind("jwt", jwtsetting);
            services.AddJwt(jwtsetting);
						
            services.AddMvc(options =>
                {
                    options.CacheProfiles.Add("Default",
                        new CacheProfile()
                        {
                            NoStore = true,
                            Duration = 0
                        });
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    options.Filters.Add(new AuthorizeFilter(policy));  
                })
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
				.AddApplicationPart(Assembly.Load(new AssemblyName("QSI.Security.Api.AspNetCore")))
                .AddApplicationPart(Assembly.Load(new AssemblyName("QSI.Notification.Api.AspNetCore"))) //Notification
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
			
			services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc($"{Configuration.GetSection("swagger:generator:doc").GetValue<string>("name")}", new Info { Title = $"{Configuration.GetSection("swagger:generator:doc:info").GetValue<string>("title")}", Version = $"{Configuration.GetSection("swagger:generator:doc:info").GetValue<string>("version")}" });
                if(Configuration.GetSection("swagger:generator").GetValue<bool>("describeAllEnumsAsStrings"))
                    c.DescribeAllEnumsAsStrings();
            });
			
			services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);
            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[] { "image/svg+xml" });
            });

            services.AddCors();

            services.AddAutoMapper(mapperConfig => {
                mapperConfig.AddProfiles(new string[] {
                    "loan",
                    "QSI.Notification.Service" //notif
                });
                mapperConfig.IgnoreUnmapped();
            });
            var setting = new DatabaseConfiguration();
            Configuration.Bind("orm", setting);
			services.AddConnection(setting);

            #region Notification Implementation
            var smsConfig = new SmsConfiguration();
            Configuration.Bind("Sms", smsConfig);
            services.AddSmsNotification(smsConfig);

            var fcmConfig = new FcmConfiguration();
            Configuration.Bind("Fcm", fcmConfig);
            services.AddFcmNotification(fcmConfig);

            var emailConfig = new EmailConfiguration();
            Configuration.Bind("Email", emailConfig);
            services.AddEmailNotification(emailConfig);
            #endregion

            #region General Notification Implementation
            builder.RegisterType<NotificationFactory>().As<INotificationFactory>();
            #endregion

            Assembly[] assemblies = new Assembly[]
            {
				Assembly.Load("QSI.Security.Repository.NHibernate"),
				Assembly.Load("QSI.Security.Service"),
				Assembly.Load("QSI.Security"),
                Assembly.Load("QSI.Notification.Repository.NHibernate"), //notif
                Assembly.Load("QSI.Notification.Service"), //notif
				Assembly.Load("loan")
            };
			builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>();
            builder.RegisterTransactionalAssemblies(assemblies, e =>
            {
                var context = e.Resolve<IHttpContextAccessor>();
                if (context.HttpContext == null)
                    return "service.user";
                return context.HttpContext.User.Identity.Name;
            });
            builder.RegisterCriteriaCondition();
            builder.RegisterGeneralHelperDao();

            #region Register Extension
            builder.RegisterUserAccountServiceWithDefaultUserAccountGenerator()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(TransactionalInterceptor));
            builder.RegisterUserAccountWorkflowWithDefaultUsernameGenerator();
            #endregion

            #region Mass Transit
            //Add nuget package QSI.MassTransit.Boot.Starter & QSI.MassTransit.Core
            MassTransitBootConfiguration masstransitBootConfiguration = new MassTransitBootConfiguration();
            Configuration.Bind("masstransit", masstransitBootConfiguration);

            builder.UseMassTransit(masstransitBootConfiguration, (cfg, host, ctx) =>
            {
                if (!cfg.GetType().GetInterfaces().Contains(typeof(IRabbitMqBusFactoryConfigurator)))
                {
                    throw new ArgumentException("Parameter host must implement IRabbitMqHost", "host");
                }
            });

            services.AddSingleton<Microsoft.Extensions.Hosting.IHostedService, BusService>();
            #endregion

            #region External Services References
            var securityUri = new Uri($"{masstransitBootConfiguration.MassTransitConfiguration.BaseUrl}/security-service");
            builder.AddSecurityRequest(securityUri);
            #endregion

            builder.AddDatabaseAuthentication();			
			builder.Populate(services);
            ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime appLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseMiddleware<MiddlewareExceptionShaper>();
            }		
			app.UseSwagger(c =>
            {
                c.RouteTemplate = $"{Configuration.GetSection("swagger:route").GetValue<string>("template")}";
                c.PreSerializeFilters.Add((swaggerDoc, httpReq) => swaggerDoc.Host = httpReq.Host.Value);
            });
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = $"{Configuration.GetSection("swagger:ui").GetValue<string>("prefix")}";
                c.DocumentTitle = $"{Configuration.GetSection("swagger:ui").GetValue<string>("documentTitle")}";
                c.SwaggerEndpoint($"{Configuration.GetSection("swagger:ui:endpoint").GetValue<string>("url")}", $"{Configuration.GetSection("swagger:ui:endpoint").GetValue<string>("name")}");
            });
            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            app.UseResponseCompression();
			app.UseSecureHeaders();
			app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
            appLifetime.ApplicationStopped.Register(() => this.ApplicationContainer.Dispose());
        }

    }
}
