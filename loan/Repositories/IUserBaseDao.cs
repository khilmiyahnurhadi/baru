﻿using loan.Models.User;
using QSI.Persistence.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Repositories
{
    public interface IUserBaseDao : BaseDao<UserBase, long>
    {
    }
}
