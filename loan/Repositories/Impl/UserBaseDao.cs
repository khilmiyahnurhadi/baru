﻿using loan.Models.User;
using QSI.ORM.NHibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Repositories.Impl
{
    public class UserBaseDao : BaseDaoNHibernate<UserBase, long>, IUserBaseDao
    {
    }
}
