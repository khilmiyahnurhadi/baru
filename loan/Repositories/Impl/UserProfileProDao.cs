﻿using loan.Models.User;
using NHibernate;
using NHibernate.Criterion;
using QSI.ORM.NHibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Repositories.Impl
{
    public class UserProfileProDao : BaseDaoNHibernate<UserProfilePro, long>, IUserProfileProDao
    {
        public UserProfilePro GetByUsernameCode(string username, params string[] fetchTables)
        {
            ICriteria criteria = Session.CreateCriteria<UserProfilePro>()
                .CreateAlias("UserProfile", "u")
                .Add(Expression.Eq("u.Username", username)); //expression use NHibernate.Criterion not System.Linq
            foreach (string fetchTable in fetchTables)
            {
                criteria.SetFetchMode(fetchTable, FetchMode.Eager);
            }

            return criteria.UniqueResult<UserProfilePro>();
        }
    }
}
