﻿using loan.Models.User;
using QSI.Persistence.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Repositories
{
    public interface IUserProfileProDao : BaseDao<UserProfilePro, long>
    {
        UserProfilePro GetByUsernameCode(string username, params string[] fetchTables);
    }
}
