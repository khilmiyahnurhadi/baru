﻿using Autofac;
using Autofac.Builder;
using loan.Services;
using loan.Services.Impl;
using loan.WorkFlows;
using loan.WorkFlows.Impl;
using loan.WorkFlows.Listeners;
using QSI.Common.Helper;
using QSI.Persistence.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Extensions
{
    public static class UserAccountExtension
    {
        public static IRegistrationBuilder<UserAccountService, ConcreteReflectionActivatorData, SingleRegistrationStyle>
            RegisterUserAccountServiceWithDefaultUserAccountGenerator(this ContainerBuilder builder)
        {
            return builder.RegisterType<UserAccountService>()
                .OnActivating(e =>
                {
                    var workflow = e.Context.Resolve<IUserAccountRegistrationWorkflow>();
                    var helperDao = e.Context.Resolve<GeneralHelperDao>();

                    IdGenerator generator = new IdGenerator<Models.User.UserBase>()
                                                .AddChunk("JT")
                                                .AddChunk(x =>
                                                {
                                                    long seq = helperDao.GetLatestSeq<Models.User.UserBase>("Username", "JT", 4);
                                                    return (seq == 0 ? "0001" : (seq + 1).ToString("0000"));
                                                });
                    e.Instance.workflow = workflow;
                    e.Instance.userAccIdGenerator = generator;
                })
                .As<IUserAccountService>();
        }

        public static IRegistrationBuilder<UserAccountRegistrationWorkflowImpl, ConcreteReflectionActivatorData, SingleRegistrationStyle>
            RegisterUserAccountWorkflowWithDefaultUsernameGenerator(this ContainerBuilder builder)
        {
            return builder.RegisterType<UserAccountRegistrationWorkflowImpl>()
                .OnActivating(e =>
                {
                    IdGenerator generator = new IdGenerator<Models.User.UserBase>()
                                               .AddChunk(x => x.Username);
                    var workflowListener = e.Context.Resolve<IUserAccountRegistrationWorkflowListener>();
                    e.Instance.workflowListener = workflowListener;
                    e.Instance.usernameGenerator = generator;
                })
                .As<IUserAccountRegistrationWorkflow>();
        }
    }
}
