
    alter table t_city 
        add constraint FKF1837851B945708F 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK169C5198E8E80A88 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK4D91C4F940ED0F5C 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK2FDDDFDA1F8D6166 
        foreign key (PostalCodeId_id) 
        references t_postalcode