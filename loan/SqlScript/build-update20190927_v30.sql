
    alter table t_city 
        add constraint FK71111C8465B92C8C 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKEFB107C5C7987357 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKFB29E043381BC8A6 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK54099E535FE71B2A 
        foreign key (PostalCodeId_id) 
        references t_postalcode