
    alter table t_city 
        add constraint FK3DA6C1C8924F10DD 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKC7A9096D116AFCC9 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK6B7FED6C449110FE 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK3DB7E12AD6521246 
        foreign key (PostalCodeId_id) 
        references t_postalcode