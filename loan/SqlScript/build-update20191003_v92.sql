
    alter table t_city 
        add constraint FKD4ECAD8E6DA6C50D 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK7F1F67DC6CB8DAF4 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK49D9090C701DE3E5 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK81565E36EF58A8D7 
        foreign key (PostalCodeId_id) 
        references t_postalcode