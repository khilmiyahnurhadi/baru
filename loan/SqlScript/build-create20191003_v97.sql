
    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKPermissionId]') and parent_object_id = OBJECT_ID(N'sec_role_permission'))
alter table sec_role_permission  drop constraint FKPermissionId


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKRoleId]') and parent_object_id = OBJECT_ID(N'sec_role_permission'))
alter table sec_role_permission  drop constraint FKRoleId


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKPermissionId]') and parent_object_id = OBJECT_ID(N'sec_user_permission'))
alter table sec_user_permission  drop constraint FKPermissionId


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKUserId]') and parent_object_id = OBJECT_ID(N'sec_user_permission'))
alter table sec_user_permission  drop constraint FKUserId


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKRoleId]') and parent_object_id = OBJECT_ID(N'sec_user_role'))
alter table sec_user_role  drop constraint FKRoleId


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKUserId]') and parent_object_id = OBJECT_ID(N'sec_user_role'))
alter table sec_user_role  drop constraint FKUserId


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[application_id]') and parent_object_id = OBJECT_ID(N'sec_user_session'))
alter table sec_user_session  drop constraint application_id


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[device_id]') and parent_object_id = OBJECT_ID(N'sec_user_session'))
alter table sec_user_session  drop constraint device_id


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[user_id]') and parent_object_id = OBJECT_ID(N'sec_user_session'))
alter table sec_user_session  drop constraint user_id


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[user_id]') and parent_object_id = OBJECT_ID(N'sec_third_party_account'))
alter table sec_third_party_account  drop constraint user_id


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[user_id]') and parent_object_id = OBJECT_ID(N'sec_user_device'))
alter table sec_user_device  drop constraint user_id


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK6918F16222055291]') and parent_object_id = OBJECT_ID(N't_city'))
alter table t_city  drop constraint FK6918F16222055291


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK227C49A7ED0E7C7A]') and parent_object_id = OBJECT_ID(N't_postalcode'))
alter table t_postalcode  drop constraint FK227C49A7ED0E7C7A


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKF81BB35357F54478]') and parent_object_id = OBJECT_ID(N't_subdistrict'))
alter table t_subdistrict  drop constraint FKF81BB35357F54478


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK_UserProfileProToUserProfile]') and parent_object_id = OBJECT_ID(N'user_profile'))
alter table user_profile  drop constraint FK_UserProfileProToUserProfile


    if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FK4380EDD387C50622]') and parent_object_id = OBJECT_ID(N't_village'))
alter table t_village  drop constraint FK4380EDD387C50622


    if exists (select * from dbo.sysobjects where id = object_id(N'sec_otp') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_otp

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_permission') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_permission

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_role') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_role

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_role_permission') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_role_permission

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_user') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_user

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_user_permission') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_user_permission

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_user_role') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_user_role

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_application') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_application

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_client') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_client

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_refresh_token') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_refresh_token

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_user_session') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_user_session

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_third_party_account') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_third_party_account

    if exists (select * from dbo.sysobjects where id = object_id(N'sec_user_device') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table sec_user_device

    if exists (select * from dbo.sysobjects where id = object_id(N't_defaultcost') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table t_defaultcost

    if exists (select * from dbo.sysobjects where id = object_id(N't_percent') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table t_percent

    if exists (select * from dbo.sysobjects where id = object_id(N't_ptkp') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table t_ptkp

    if exists (select * from dbo.sysobjects where id = object_id(N't_simulation') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table t_simulation

    if exists (select * from dbo.sysobjects where id = object_id(N't_city') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table t_city

    if exists (select * from dbo.sysobjects where id = object_id(N't_postalcode') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table t_postalcode

    if exists (select * from dbo.sysobjects where id = object_id(N't_province') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table t_province

    if exists (select * from dbo.sysobjects where id = object_id(N't_subdistrict') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table t_subdistrict

    if exists (select * from dbo.sysobjects where id = object_id(N'user_base') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table user_base

    if exists (select * from dbo.sysobjects where id = object_id(N'user_profile') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table user_profile

    if exists (select * from dbo.sysobjects where id = object_id(N't_village') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table t_village

    if exists (select * from dbo.sysobjects where id = object_id(N'notification') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table notification

    create table sec_otp (
        id BIGINT IDENTITY NOT NULL,
       timestamp DATETIME2 null,
       actor NVARCHAR(255) null,
       application NVARCHAR(30) not null,
       verification_id NVARCHAR(20) null,
       reference NVARCHAR(50) null,
       enabled BIT null,
       primary key (id)
    )

    create table sec_permission (
        Id INT IDENTITY NOT NULL,
       timestamp DATETIME2 null,
       actor NVARCHAR(255) null,
       name NVARCHAR(30) not null,
       description NVARCHAR(255) null,
       primary key (Id)
    )

    create table sec_role (
        Id INT IDENTITY NOT NULL,
       timestamp DATETIME2 null,
       actor NVARCHAR(255) null,
       name NVARCHAR(30) not null,
       description NVARCHAR(255) null,
       primary key (Id)
    )

    create table sec_role_permission (
        role_id INT not null,
       permission_id INT not null,
       primary key (role_id, permission_id)
    )

    create table sec_user (
        Id BIGINT IDENTITY NOT NULL,
       timestamp DATETIME2 null,
       actor NVARCHAR(255) null,
       username NVARCHAR(50) not null,
       password NVARCHAR(150) not null,
       account_non_locked BIT not null,
       enabled BIT not null,
       account_non_expired BIT not null,
       credential_non_expired BIT not null,
       last_login DATETIME2 null,
       try_count INT not null,
       status INT not null,
       primary key (Id)
    )

    create table sec_user_permission (
        user_id BIGINT not null,
       permission_id INT not null,
       primary key (user_id, permission_id)
    )

    create table sec_user_role (
        user_id BIGINT not null,
       role_id INT not null,
       primary key (user_id, role_id)
    )

    create table sec_application (
        id BIGINT IDENTITY NOT NULL,
       name NVARCHAR(255) not null unique,
       primary key (id)
    )

    create table sec_client (
        id NVARCHAR(255) not null,
       name NVARCHAR(100) not null,
       secret NVARCHAR(255) not null,
       refresh_token_life_time INT null,
       active BIT null,
       allowed_origin NVARCHAR(100) null,
       application_type INT null,
       primary key (id)
    )

    create table sec_refresh_token (
        id NVARCHAR(255) not null,
       client_id NVARCHAR(50) not null,
       subject NVARCHAR(50) not null,
       issued_utc DATETIME2 null,
       expires_utc DATETIME2 null,
       protected_ticket NVARCHAR(255) not null,
       primary key (id),
      unique (client_id, subject)
    )

    create table sec_user_session (
        id NVARCHAR(255) not null,
       timestamp DATETIME2 null,
       actor NVARCHAR(255) null,
       expired_time DATETIME2 null,
       login_time DATETIME2 not null,
       logout_time DATETIME2 null,
       token NVARCHAR(255) null,
       application_id BIGINT not null,
       device_id NVARCHAR(255) null,
       user_id BIGINT null,
       primary key (id)
    )

    create table sec_third_party_account (
        id BIGINT IDENTITY NOT NULL,
       timestamp DATETIME2 null,
       actor NVARCHAR(255) null,
       application NVARCHAR(255) not null,
       account NVARCHAR(255) not null,
       user_id BIGINT not null,
       primary key (id)
    )

    create table sec_user_device (
        id NVARCHAR(255) not null,
       timestamp DATETIME2 null,
       actor NVARCHAR(255) null,
       active BIT null,
       name NVARCHAR(255) not null,
       os NVARCHAR(255) null,
       platform NVARCHAR(255) null,
       version NVARCHAR(255) null,
       user_id BIGINT not null,
       primary key (id)
    )

    create table t_defaultcost (
        Id BIGINT IDENTITY NOT NULL,
       CostName NVARCHAR(255) not null,
       Cost BIGINT not null,
       primary key (Id)
    )

    create table t_percent (
        Id BIGINT IDENTITY NOT NULL,
       CostName NVARCHAR(255) not null,
       PercentNumber DECIMAL(19,5) not null,
       primary key (Id)
    )

    create table t_ptkp (
        Id BIGINT IDENTITY NOT NULL,
       Status BIT not null,
       MateHasIncome BIT not null,
       Burden SMALLINT not null,
       TotalPTKP BIGINT not null,
       primary key (Id)
    )

    create table t_simulation (
        Id BIGINT IDENTITY NOT NULL,
       PropertyPrices BIGINT not null,
       PTKP BIGINT not null,
       DownPayment BIGINT not null,
       LoanInterestCost DECIMAL(19,5) not null,
       HomeInsuranceCost DECIMAL(19,5) not null,
       LifeInsuranceCost BIGINT not null,
       AdministrativeCost DECIMAL(19,5) not null,
       BPHTB DECIMAL(19,5) not null,
       NotaryFee DECIMAL(19,5) not null,
       PNPB BIGINT not null,
       StampDuty BIGINT not null,
       primary key (Id)
    )

    create table t_city (
        Id BIGINT IDENTITY NOT NULL,
       CityName NVARCHAR(255) not null,
       provinceid_id BIGINT not null,
       primary key (Id)
    )

    create table t_postalcode (
        Id BIGINT IDENTITY NOT NULL,
       PostalCodeNumber NVARCHAR(255) not null,
       SubDistrictId_id BIGINT not null,
       primary key (Id)
    )

    create table t_province (
        Id BIGINT IDENTITY NOT NULL,
       ProvinceName NVARCHAR(255) not null,
       primary key (Id)
    )

    create table t_subdistrict (
        Id BIGINT IDENTITY NOT NULL,
       SubDistrictName NVARCHAR(255) not null,
       CityId_id BIGINT not null,
       primary key (Id)
    )

    create table user_base (
        Id BIGINT IDENTITY NOT NULL,
       timestamp DATETIME2 null,
       actor NVARCHAR(255) null,
       Username NVARCHAR(255) not null,
       FullName NVARCHAR(255) not null,
       PhoneNumber NVARCHAR(255) not null,
       Email NVARCHAR(255) not null,
       primary key (Id)
    )

    create table user_profile (
        Id BIGINT IDENTITY NOT NULL,
       UserId NVARCHAR(255) not null,
       Username NVARCHAR(255) not null,
       Status INT not null,
       UserProfile_id BIGINT not null,
       primary key (Id)
    )

    create table t_village (
        Id BIGINT IDENTITY NOT NULL,
       VillageName NVARCHAR(255) not null,
       PostalCodeId_id BIGINT not null,
       primary key (Id)
    )

    create table notification (
        id BIGINT IDENTITY NOT NULL,
       timestamp DATETIME2 null,
       actor NVARCHAR(255) null,
       is_read BIT not null,
       recipient NVARCHAR(255) not null,
       content NVARCHAR(255) not null,
       title NVARCHAR(255) not null,
       created_time DATETIME2 not null,
       primary key (id)
    )

    alter table sec_role_permission 
        add constraint FKPermissionId 
        foreign key (permission_id) 
        references sec_permission

    alter table sec_role_permission 
        add constraint FKRoleId 
        foreign key (role_id) 
        references sec_role

    alter table sec_user_permission 
        add constraint FKPermissionId 
        foreign key (permission_id) 
        references sec_permission

    alter table sec_user_permission 
        add constraint FKUserId 
        foreign key (user_id) 
        references sec_user

    alter table sec_user_role 
        add constraint FKRoleId 
        foreign key (role_id) 
        references sec_role

    alter table sec_user_role 
        add constraint FKUserId 
        foreign key (user_id) 
        references sec_user

    alter table sec_user_session 
        add constraint application_id 
        foreign key (application_id) 
        references sec_application

    alter table sec_user_session 
        add constraint device_id 
        foreign key (device_id) 
        references sec_user_device

    alter table sec_user_session 
        add constraint user_id 
        foreign key (user_id) 
        references sec_user

    alter table sec_third_party_account 
        add constraint user_id 
        foreign key (user_id) 
        references sec_user

    alter table sec_user_device 
        add constraint user_id 
        foreign key (user_id) 
        references sec_user

    alter table t_city 
        add constraint FK6918F16222055291 
        foreign key (provinceid_id) 
        references t_province

    alter table t_postalcode 
        add constraint FK227C49A7ED0E7C7A 
        foreign key (SubDistrictId_id) 
        references t_subdistrict

    alter table t_subdistrict 
        add constraint FKF81BB35357F54478 
        foreign key (CityId_id) 
        references t_city

    alter table user_profile 
        add constraint FK_UserProfileProToUserProfile 
        foreign key (UserProfile_id) 
        references user_base

    alter table t_village 
        add constraint FK4380EDD387C50622 
        foreign key (PostalCodeId_id) 
        references t_postalcode
