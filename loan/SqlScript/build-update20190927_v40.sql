
    alter table t_city 
        add constraint FK75D556E22B9F83CE 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK318883CE4FDCE3F3 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK8C7E007BDAB1182A 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK4BED9B73548F59FD 
        foreign key (PostalCodeId_id) 
        references t_postalcode