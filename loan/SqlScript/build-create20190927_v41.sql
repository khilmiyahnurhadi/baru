
    drop table if exists sec_otp cascade

    drop table if exists sec_permission cascade

    drop table if exists sec_role cascade

    drop table if exists sec_role_permission cascade

    drop table if exists sec_user cascade

    drop table if exists sec_user_permission cascade

    drop table if exists sec_user_role cascade

    drop table if exists sec_application cascade

    drop table if exists sec_client cascade

    drop table if exists sec_refresh_token cascade

    drop table if exists sec_user_session cascade

    drop table if exists sec_third_party_account cascade

    drop table if exists sec_user_device cascade

    drop table if exists t_defaultcost cascade

    drop table if exists t_percent cascade

    drop table if exists t_ptkp cascade

    drop table if exists t_simulation cascade

    drop table if exists t_city cascade

    drop table if exists t_postalcode cascade

    drop table if exists t_province cascade

    drop table if exists t_subdistrict cascade

    drop table if exists user_base cascade

    drop table if exists user_profile cascade

    drop table if exists t_village cascade

    drop table if exists notification cascade

    create table sec_otp (
        id  bigserial,
       timestamp timestamp,
       actor varchar(255),
       application varchar(30) not null,
       verification_id varchar(20),
       reference varchar(50),
       enabled boolean,
       primary key (id)
    )

    create table sec_permission (
        Id  serial,
       timestamp timestamp,
       actor varchar(255),
       name varchar(30) not null,
       description varchar(255),
       primary key (Id)
    )

    create table sec_role (
        Id  serial,
       timestamp timestamp,
       actor varchar(255),
       name varchar(30) not null,
       description varchar(255),
       primary key (Id)
    )

    create table sec_role_permission (
        role_id int4 not null,
       permission_id int4 not null,
       primary key (role_id, permission_id)
    )

    create table sec_user (
        Id  bigserial,
       timestamp timestamp,
       actor varchar(255),
       username varchar(50) not null,
       password varchar(150) not null,
       account_non_locked boolean not null,
       enabled boolean not null,
       account_non_expired boolean not null,
       credential_non_expired boolean not null,
       last_login timestamp,
       try_count int4 not null,
       status int4 not null,
       primary key (Id)
    )

    create table sec_user_permission (
        user_id int8 not null,
       permission_id int4 not null,
       primary key (user_id, permission_id)
    )

    create table sec_user_role (
        user_id int8 not null,
       role_id int4 not null,
       primary key (user_id, role_id)
    )

    create table sec_application (
        id  bigserial,
       name varchar(255) not null unique,
       primary key (id)
    )

    create table sec_client (
        id varchar(255) not null,
       name varchar(100) not null,
       secret varchar(255) not null,
       refresh_token_life_time int4,
       active boolean,
       allowed_origin varchar(100),
       application_type int4,
       primary key (id)
    )

    create table sec_refresh_token (
        id varchar(255) not null,
       client_id varchar(50) not null,
       subject varchar(50) not null,
       issued_utc timestamp,
       expires_utc timestamp,
       protected_ticket varchar(255) not null,
       primary key (id),
      unique (client_id, subject)
    )

    create table sec_user_session (
        id varchar(255) not null,
       timestamp timestamp,
       actor varchar(255),
       expired_time timestamp,
       login_time timestamp not null,
       logout_time timestamp,
       token varchar(255),
       application_id int8 not null,
       device_id varchar(255),
       user_id int8,
       primary key (id)
    )

    create table sec_third_party_account (
        id  bigserial,
       timestamp timestamp,
       actor varchar(255),
       application varchar(255) not null,
       account varchar(255) not null,
       user_id int8 not null,
       primary key (id)
    )

    create table sec_user_device (
        id varchar(255) not null,
       timestamp timestamp,
       actor varchar(255),
       active boolean,
       name varchar(255) not null,
       os varchar(255),
       platform varchar(255),
       version varchar(255),
       user_id int8 not null,
       primary key (id)
    )

    create table t_defaultcost (
        Id  bigserial,
       CostName varchar(255) not null,
       Cost int8 not null,
       primary key (Id)
    )

    create table t_percent (
        Id  bigserial,
       CostName varchar(255) not null,
       PercentNumber decimal(19,5) not null,
       primary key (Id)
    )

    create table t_ptkp (
        Id  bigserial,
       Status boolean not null,
       MateHasIncome boolean not null,
       Burden int2 not null,
       TotalPTKP int8 not null,
       primary key (Id)
    )

    create table t_simulation (
        Id  bigserial,
       PropertyPrices int8 not null,
       PTKP int8 not null,
       DownPayment int8 not null,
       LoanInterestCost decimal(19,5) not null,
       HomeInsuranceCost decimal(19,5) not null,
       LifeInsuranceCost int8 not null,
       AdministrativeCost decimal(19,5) not null,
       BPHTB decimal(19,5) not null,
       NotaryFee decimal(19,5) not null,
       PNPB int8 not null,
       StampDuty int8 not null,
       primary key (Id)
    )

    create table t_city (
        Id  bigserial,
       CityName varchar(255) not null,
       ProvinceId_id int8 not null,
       primary key (Id)
    )

    create table t_postalcode (
        Id  bigserial,
       PostalCodeNumber varchar(255) not null,
       SubDistrictId_id int8 not null,
       primary key (Id)
    )

    create table t_province (
        Id  bigserial,
       ProvinceName varchar(255) not null,
       primary key (Id)
    )

    create table t_subdistrict (
        Id  bigserial,
       SubDistrictName varchar(255) not null,
       CityId_id int8 not null,
       primary key (Id)
    )

    create table user_base (
        Id  bigserial,
       timestamp timestamp,
       actor varchar(255),
       Username varchar(255) not null,
       FullName varchar(255) not null,
       PhoneNumber varchar(255) not null,
       Email varchar(255) not null,
       primary key (Id)
    )

    create table user_profile (
        Id  bigserial,
       UserId varchar(255) not null,
       Username varchar(255) not null,
       Status int4 not null,
       UserBase_id int8 not null,
       primary key (Id)
    )

    create table t_village (
        Id  bigserial,
       VillageName varchar(255) not null,
       PostalCodeId_id int8 not null,
       primary key (Id)
    )

    create table notification (
        id  bigserial,
       timestamp timestamp,
       actor varchar(255),
       is_read boolean not null,
       recipient varchar(255) not null,
       content varchar(255) not null,
       title varchar(255) not null,
       created_time timestamp not null,
       primary key (id)
    )

    alter table sec_role_permission 
        add constraint FKPermissionId 
        foreign key (permission_id) 
        references sec_permission

    alter table sec_role_permission 
        add constraint FKRoleId 
        foreign key (role_id) 
        references sec_role

    alter table sec_user_permission 
        add constraint FKPermissionId 
        foreign key (permission_id) 
        references sec_permission

    alter table sec_user_permission 
        add constraint FKUserId 
        foreign key (user_id) 
        references sec_user

    alter table sec_user_role 
        add constraint FKRoleId 
        foreign key (role_id) 
        references sec_role

    alter table sec_user_role 
        add constraint FKUserId 
        foreign key (user_id) 
        references sec_user

    alter table sec_user_session 
        add constraint application_id 
        foreign key (application_id) 
        references sec_application

    alter table sec_user_session 
        add constraint device_id 
        foreign key (device_id) 
        references sec_user_device

    alter table sec_user_session 
        add constraint user_id 
        foreign key (user_id) 
        references sec_user

    alter table sec_third_party_account 
        add constraint user_id 
        foreign key (user_id) 
        references sec_user

    alter table sec_user_device 
        add constraint user_id 
        foreign key (user_id) 
        references sec_user

    alter table t_city 
        add constraint FKF7F82D58BD4E8 
        foreign key (ProvinceId_id) 
        references t_province

    alter table t_postalcode 
        add constraint FK201D75BAECBB2FAE 
        foreign key (SubDistrictId_id) 
        references t_subdistrict

    alter table t_subdistrict 
        add constraint FKB780D50568067BA 
        foreign key (CityId_id) 
        references t_city

    alter table user_profile 
        add constraint FK_UserProfileProToUserBase 
        foreign key (UserBase_id) 
        references user_base

    alter table t_village 
        add constraint FK142096C3121161E6 
        foreign key (PostalCodeId_id) 
        references t_postalcode
