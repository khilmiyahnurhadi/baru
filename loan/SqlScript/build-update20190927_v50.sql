
    alter table t_city 
        add constraint FK9109461EBD74F1E6 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK5297050B1A5E8AC7 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKF684E3C358377694 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK17AE0E2010E48DFB 
        foreign key (PostalCodeId_id) 
        references t_postalcode