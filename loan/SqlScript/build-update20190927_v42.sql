
    alter table t_city 
        add constraint FKF7F82D58BD4E8 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK201D75BAECBB2FAE 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB780D50568067BA 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK142096C3121161E6 
        foreign key (PostalCodeId_id) 
        references t_postalcode