
    alter table t_city 
        add constraint FKC7316EB5115F53D 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKE7DFCE9A1E8BFA74 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB5A69397202D2E35 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK4897462D8BD44113 
        foreign key (PostalCodeId_id) 
        references t_postalcode