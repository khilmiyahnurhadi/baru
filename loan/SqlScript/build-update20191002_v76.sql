
    alter table t_city 
        add constraint FKE58C4FBE7026A846 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK4CDA9B0C10C5E26A 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKEFF5D61B2ED7D9C2 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK61F76B2912FA06C6 
        foreign key (PostalCodeId_id) 
        references t_postalcode