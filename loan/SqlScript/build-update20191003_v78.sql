
    alter table t_city 
        add constraint FK7996ED7C8F347D02 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK8EEE2B513028149A 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKE0CA298AD65DC2F6 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK4FD2281DCF94954C 
        foreign key (PostalCodeId_id) 
        references t_postalcode