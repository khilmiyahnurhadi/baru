
    alter table t_city 
        add constraint FKC403BCDD5F92EE47 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK9E0A5F6E3C253F8C 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKD2288DAB605B06B3 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK9107E91F81886F83 
        foreign key (PostalCodeId_id) 
        references t_postalcode