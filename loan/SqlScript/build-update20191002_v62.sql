
    alter table t_city 
        add constraint FKC9404831E527B53C 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKF033BDA87EC51C35 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKCF23A39EB415392E 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKD3C9CF43322EA416 
        foreign key (PostalCodeId_id) 
        references t_postalcode