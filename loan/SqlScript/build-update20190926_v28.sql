
    alter table t_city 
        add constraint FK845DBDDFB78210F8 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKE0B8F1F452EB2A66 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKD6D5EACFEBD0904 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKBD08759E32B7D0E4 
        foreign key (PostalCodeId_id) 
        references t_postalcode