
    alter table t_city 
        add constraint FK5635304225ACA0E9 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKE4080C88A7299F33 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK4ED6DB2327D27FB2 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK3AE47309C523BC33 
        foreign key (PostalCodeId_id) 
        references t_postalcode