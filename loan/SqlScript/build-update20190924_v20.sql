
    alter table t_city 
        add constraint FKC1DE8F9F4B1FF87C 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK71FAB12E395BEF15 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKF982BBBA50363242 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKE8D482524D0CD8D4 
        foreign key (PostalCodeId_id) 
        references t_postalcode