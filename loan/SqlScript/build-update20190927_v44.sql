
    alter table t_city 
        add constraint FK6D6F932BAA51F6BC 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKE811F4E25BBA6307 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB92AF7A15134967F 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK173D9DBA5CB8D7F7 
        foreign key (PostalCodeId_id) 
        references t_postalcode