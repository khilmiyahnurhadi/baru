
    alter table t_city 
        add constraint FK38C95AF7F48391EF 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK17461F35CF474B1B 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK3907EA73B02985D2 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK7357053E177431F7 
        foreign key (PostalCodeId_id) 
        references t_postalcode