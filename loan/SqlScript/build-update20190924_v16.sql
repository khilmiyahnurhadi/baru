
    alter table t_city 
        add constraint FKCFDF2319EC8E00A1 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKCF3BDAE89EA8DEE3 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKAD790967A40CB9DE 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKAFFA1A16A40EC311 
        foreign key (PostalCodeId_id) 
        references t_postalcode