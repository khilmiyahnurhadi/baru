
    alter table t_city 
        add constraint FKEAD399A25A31489B 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKE764068C8A6C8BC7 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKDC25FF6F1BC5EBC9 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKE40BB1193E8082EB 
        foreign key (PostalCodeId_id) 
        references t_postalcode