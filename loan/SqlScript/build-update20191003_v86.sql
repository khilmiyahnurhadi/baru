
    alter table t_city 
        add constraint FKD7837279F3289539 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK9EAA98C162BA28C6 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK89BC8CBBC5C683B8 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK1494DE765F72FFD3 
        foreign key (PostalCodeId_id) 
        references t_postalcode