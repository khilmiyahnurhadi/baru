
    alter table t_city 
        add constraint FK5E1E48219868994 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK4D0D3B31BDDE177 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK6E53B79588B5D1D9 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK2E410F34B06013CA 
        foreign key (PostalCodeId_id) 
        references t_postalcode