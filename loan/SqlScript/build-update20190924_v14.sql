
    alter table t_city 
        add constraint FKB87BEA9A105C7EB1 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK3A9759EEC502426 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK7315D8454A957D6D 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK698F9AD343F9D171 
        foreign key (PostalCodeId_id) 
        references t_postalcode