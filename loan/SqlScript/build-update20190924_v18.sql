
    alter table t_city 
        add constraint FK88F832D065BB9D6F 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK235CB6B2FD75E5BA 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK1D028DED8F85D875 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK10E7A1EDE6B2872E 
        foreign key (PostalCodeId_id) 
        references t_postalcode