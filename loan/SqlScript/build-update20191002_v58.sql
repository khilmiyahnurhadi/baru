
    alter table t_city 
        add constraint FKBABA63D9AF4E0B97 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK12DFE5803546F11 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK9D16CF457A355198 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK1C9260118164CB8A 
        foreign key (PostalCodeId_id) 
        references t_postalcode