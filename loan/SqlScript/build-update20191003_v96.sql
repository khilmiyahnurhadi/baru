
    alter table t_city 
        add constraint FKDCAA6E6BFB9873E 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK94DD7C2725F6AFEA 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKE396FBC11ACEF2A7 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKE8669A439E7DA1E0 
        foreign key (PostalCodeId_id) 
        references t_postalcode