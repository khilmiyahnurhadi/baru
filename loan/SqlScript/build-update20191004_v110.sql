
    create table user_base (
        Id  bigserial,
       timestamp timestamp,
       actor varchar(255),
       Username varchar(255) not null,
       FullName varchar(255) not null,
       PhoneNumber varchar(255) not null,
       Email varchar(255) not null,
       primary key (Id)
    )
    create table user_profile (
        Id  bigserial,
       UserId varchar(255) not null,
       Username varchar(255) not null,
       Status int4 not null,
       UserProfile_id int8 not null,
       primary key (Id)
    )
    create table notification (
        id  bigserial,
       timestamp timestamp,
       actor varchar(255),
       is_read boolean not null,
       recipient varchar(255) not null,
       content varchar(255) not null,
       title varchar(255) not null,
       created_time timestamp not null,
       primary key (id)
    )
    alter table t_city 
        add constraint FK836F91A59B3280C8 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK8253F37F8097B8C9 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK53AD2C32C95CC38 
        foreign key (CityId_id) 
        references t_city
    alter table user_profile 
        add constraint FK_UserProfileProToUserProfile 
        foreign key (UserProfile_id) 
        references user_base
    alter table t_village 
        add constraint FKFCF88E0153F876B8 
        foreign key (PostalCodeId_id) 
        references t_postalcode