
    alter table t_city 
        add constraint FK617A78D124D7F2C7 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK333F4147CDEBF57 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKBD1C25C85536D2EF 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKA1B5C1871B4D06C5 
        foreign key (PostalCodeId_id) 
        references t_postalcode