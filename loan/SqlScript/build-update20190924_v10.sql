
    alter table t_city 
        add constraint FK85E1165CD57C732A 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK97A54BAFCFAC457D 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKA95F27A8FDA7408E 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKBD84308419EE11CB 
        foreign key (PostalCodeId_id) 
        references t_postalcode