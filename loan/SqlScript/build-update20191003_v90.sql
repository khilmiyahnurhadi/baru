
    alter table t_city 
        add constraint FKC943E0C3C706DF78 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK78934F40B714A8DC 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKF566455364FFE1DD 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK170B97767FE606D3 
        foreign key (PostalCodeId_id) 
        references t_postalcode