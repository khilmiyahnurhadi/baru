
    alter table t_city 
        add constraint FK3E8E4ED3B9E828D3 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK9D05CB9E227BE2A1 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK127F182AB4CD149C 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKF6879D4E313C5C4E 
        foreign key (PostalCodeId_id) 
        references t_postalcode