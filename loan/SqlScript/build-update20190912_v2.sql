
    alter table t_city 
        add constraint FKE402A1BE9AF8547E 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKCCF3B073B06AFD95 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK7A0BC7ED81E02701 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKFDAF52253559D64B 
        foreign key (PostalCodeId_id) 
        references t_postalcode