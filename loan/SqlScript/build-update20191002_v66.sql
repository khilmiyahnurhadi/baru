
    alter table t_city 
        add constraint FKBF774575AAE752EF 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK9EC8C10DA4533E77 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKCD9533DCDDF56AB2 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK65003A41A87EA990 
        foreign key (PostalCodeId_id) 
        references t_postalcode