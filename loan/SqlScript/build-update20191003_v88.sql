
    alter table t_city 
        add constraint FKD879275751E56792 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK97FA36026A449134 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB3620801CEF8DC6E 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKD8C78DE9BA8FC57A 
        foreign key (PostalCodeId_id) 
        references t_postalcode