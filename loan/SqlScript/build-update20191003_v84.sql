
    alter table t_city 
        add constraint FKE3325C558909B5DE 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKDAE68945CE439556 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK903EA0A618EB4433 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKFBD22C524852210E 
        foreign key (PostalCodeId_id) 
        references t_postalcode