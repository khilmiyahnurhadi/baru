
    alter table t_city 
        add constraint FK3F02E267871F0B63 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKEA9EEE26DCFF2957 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKA1B3B0A6B869BD34 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK3A0C3A53C027E9EB 
        foreign key (PostalCodeId_id) 
        references t_postalcode