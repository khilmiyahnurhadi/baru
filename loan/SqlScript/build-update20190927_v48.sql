
    alter table user_profile 
        add column UserProfile_id int8
    alter table t_city 
        add constraint FK98B6E7FFE6449FC1 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK56AD5E01FA01A319 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB073ED0299335EC3 
        foreign key (CityId_id) 
        references t_city
    alter table user_profile 
        add constraint FK_UserProfileProToUserProfile 
        foreign key (UserProfile_id) 
        references user_base
    alter table t_village 
        add constraint FK252D0A2936D9C45F 
        foreign key (PostalCodeId_id) 
        references t_postalcode