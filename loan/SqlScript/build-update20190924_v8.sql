
    alter table t_city 
        add constraint FK3689DB4A2C155E4E 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK9F68EA9D6AE838C0 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKC23CC43DA47B0FC1 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKD02CBAA1E78C85A0 
        foreign key (PostalCodeId_id) 
        references t_postalcode