
    alter table t_city 
        add constraint FK56B66D81A3D6D89E 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK43BDED2F6875601D 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK712D1BA5605DA351 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK6816A788CED298EE 
        foreign key (PostalCodeId_id) 
        references t_postalcode