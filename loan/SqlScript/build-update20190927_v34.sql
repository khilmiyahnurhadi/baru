
    alter table t_city 
        add constraint FK50A03B4CEC303DE8 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKDC98430E7919EA42 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK4A76F386253D4055 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK39E518879212B07E 
        foreign key (PostalCodeId_id) 
        references t_postalcode