
    alter table t_city 
        add constraint FK4E1068B99E82E28B 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK39276968AB26B730 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKDC161423FFE09E91 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK6D5F61725DD91C10 
        foreign key (PostalCodeId_id) 
        references t_postalcode