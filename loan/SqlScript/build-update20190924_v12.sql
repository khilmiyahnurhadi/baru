
    alter table t_city 
        add constraint FK56184F7874D61DBF 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKEFDDD7DAC1D5975E 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKC55764C8B4D76246 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKE9915C4E9380582 
        foreign key (PostalCodeId_id) 
        references t_postalcode