
    alter table t_city 
        add constraint FKB33F320B74AEBEED 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK1091CC4113954F39 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB3C24565A377A99F 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK6EADC3DB27F836EF 
        foreign key (PostalCodeId_id) 
        references t_postalcode