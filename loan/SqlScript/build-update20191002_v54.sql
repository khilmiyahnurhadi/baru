
    alter table t_city 
        add constraint FK4C6DD9E218CB460E 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK2C457FA854E48C4B 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKCEB4CD7689134A 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK5EB53C0AB3491130 
        foreign key (PostalCodeId_id) 
        references t_postalcode