
    create table user_base (
        Id  bigserial,
       timestamp timestamp,
       actor varchar(255),
       Username varchar(255) not null,
       FullName varchar(255) not null,
       PhoneNumber varchar(255) not null,
       Email varchar(255) not null,
       primary key (Id)
    )
    create table user_profile (
        Id  bigserial,
       UserId varchar(255) not null,
       Username varchar(255) not null,
       Status int4 not null,
       UserBase_id int8 not null,
       primary key (Id)
    )
    create table notification (
        id  bigserial,
       timestamp timestamp,
       actor varchar(255),
       is_read boolean not null,
       recipient varchar(255) not null,
       content varchar(255) not null,
       title varchar(255) not null,
       created_time timestamp not null,
       primary key (id)
    )
    alter table t_city 
        add constraint FKB9093524F1B94E51 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKCA95C36F723E6BDB 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKABA7E1F7C7A21666 
        foreign key (CityId_id) 
        references t_city
    alter table user_profile 
        add constraint FK_UserProfileProToUserBase 
        foreign key (UserBase_id) 
        references user_base
    alter table t_village 
        add constraint FK141A0EBB3A5587F1 
        foreign key (PostalCodeId_id) 
        references t_postalcode