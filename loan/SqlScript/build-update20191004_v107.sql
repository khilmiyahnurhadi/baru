
    alter table t_city 
        add constraint FK372624004619B54B 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKDC99D580DCCA004E 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK8344EF2AF95B803D 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK5C89A8B37FE024C7 
        foreign key (PostalCodeId_id) 
        references t_postalcode