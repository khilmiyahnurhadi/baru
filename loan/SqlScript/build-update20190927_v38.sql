
    alter table t_city 
        add constraint FK8E99AA763AF378BB 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK1EA318D979065DB1 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB4EE87B5CE057F51 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK191B58121C159A9 
        foreign key (PostalCodeId_id) 
        references t_postalcode