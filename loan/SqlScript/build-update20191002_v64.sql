
    alter table t_city 
        add constraint FK12D97F0A96D3E97B 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKB9237E1278951997 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK3F3EF470241B2BA 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK565A77517C2D2A25 
        foreign key (PostalCodeId_id) 
        references t_postalcode