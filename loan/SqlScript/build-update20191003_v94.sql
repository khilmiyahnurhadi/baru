
    alter table t_city 
        add constraint FK10C56F0D445D569A 
        foreign key (provinceid_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK2B0F0F0DC6A9CABD 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKCEC7C43E59D9D9D1 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK9DD484D7E36B817 
        foreign key (PostalCodeId_id) 
        references t_postalcode