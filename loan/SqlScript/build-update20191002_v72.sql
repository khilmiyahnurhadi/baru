
    alter table t_city 
        add constraint FK436D910180EF4178 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKC2F3F6483E2895C6 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK51181C4D327E048B 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKE2458AEA402F6EC0 
        foreign key (PostalCodeId_id) 
        references t_postalcode