
    alter table t_city 
        add constraint FKA95285F1907A7B34 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK5837C2C5DB3DA0C8 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKE1FC16A8296490F9 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKC2CC84F42C9A1D3E 
        foreign key (PostalCodeId_id) 
        references t_postalcode