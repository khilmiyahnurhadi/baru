
    alter table t_city 
        add constraint FKD20F943179076DA 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK57393F8347E673C 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKF7AE1848237AD6 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK4EEDC2A336A2AA8 
        foreign key (PostalCodeId_id) 
        references t_postalcode