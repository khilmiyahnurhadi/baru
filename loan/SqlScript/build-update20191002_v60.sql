
    alter table t_city 
        add constraint FK3ADF71C1EB55CF10 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKB3E6F549AE9DBEBB 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK4C267D506C45C38F 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKDA51EAF7B048A582 
        foreign key (PostalCodeId_id) 
        references t_postalcode