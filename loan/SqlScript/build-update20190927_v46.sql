
    alter table t_city 
        add constraint FKA16AB5606E8FA901 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK3E5047A8662873A9 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB61547A1F3278F5B 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK8D0EF11D70A1EF25 
        foreign key (PostalCodeId_id) 
        references t_postalcode