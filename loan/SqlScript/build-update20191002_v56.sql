
    alter table t_city 
        add constraint FKB8958D00C0CE345D 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK55A7B60F228A70BB 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK375DE6E7895FA265 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK749B8A8D5AE1EC22 
        foreign key (PostalCodeId_id) 
        references t_postalcode