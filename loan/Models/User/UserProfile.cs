﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models.User
{
    public class UserProfile : Entity<long>
    {
        public virtual string Name { set; get; }
        public virtual string PhoneNumber { set; get; }
        public virtual string Email { set; get; }

        public override bool Equals(object obj)
        {
            var profile = obj as UserProfile;
            return profile != null &&
                   Name == profile.Name &&
                   PhoneNumber == profile.PhoneNumber &&
                   Email == profile.Email;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, PhoneNumber, Email);
        }
    }
}
