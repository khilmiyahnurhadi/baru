﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models.User
{
    public class UserProfilePro : Entity<long>
    {
        public virtual UserBase UserProfile { set; get; }
        public virtual string UserId { set; get; }
        public virtual string Username { set; get; }
        public virtual RecordStatus Status { set; get; }

        public override bool Equals(object obj)
        {
            var pro = obj as UserProfilePro;
            return pro != null &&
                   EqualityComparer<UserBase>.Default.Equals(UserProfile, pro.UserProfile) &&
                   UserId == pro.UserId &&
                   Username == pro.Username &&
                   Status == pro.Status;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine( UserProfile, UserId, Username, Status);
        }
    }
}
