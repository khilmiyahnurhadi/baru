﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models.User
{
    public class UserBase : AuditedEntity<long>
    {
        public virtual string Username { set; get; }
        public virtual string FullName { set; get; }
        public virtual string Email { set; get; }
        public virtual string PhoneNumber { set; get; }

        public override bool Equals(object obj)
        {
            var @base = obj as UserBase;
            return @base != null &&
                   Username == @base.Username &&
                   FullName == @base.FullName &&
                   Email == @base.Email &&
                   PhoneNumber == @base.PhoneNumber;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine( Username, FullName, Email, PhoneNumber);
        }
    }
}
