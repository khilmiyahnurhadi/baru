﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models.Calculators
{
    public class DefaultCost : Entity<long>
    {
        public virtual string CostName { set; get; }
        public virtual Int64 Cost { set; get; }

        public override bool Equals(object obj)
        {
            var cost = obj as DefaultCost;
            return cost != null &&
                   CostName == cost.CostName &&
                   Cost == cost.Cost;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CostName, Cost);
        }
    }
}
