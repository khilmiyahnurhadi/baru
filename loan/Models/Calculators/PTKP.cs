﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models.Calculators
{
    public class PTKP : Entity<long>
    {
        public virtual bool Status { set; get; }
        public virtual bool MateHasIncome { set; get; }
        public virtual Int16 Burden { set; get; }
        public virtual Int64 TotalPTKP { set; get; }

        public override bool Equals(object obj)
        {
            var pTKP = obj as PTKP;
            return pTKP != null &&
                   Status == pTKP.Status &&
                   MateHasIncome == pTKP.MateHasIncome &&
                   Burden == pTKP.Burden &&
                   TotalPTKP == pTKP.TotalPTKP;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Status, MateHasIncome, Burden, TotalPTKP);
        }
    }
}
