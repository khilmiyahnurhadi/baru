﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models.Calculators
{
    public class Percent : Entity<long>
    {
        public virtual string CostName { set; get; }
        public virtual decimal PercentNumber { set; get; }

        public override bool Equals(object obj)
        {
            var percent = obj as Percent;
            return percent != null &&
                   CostName == percent.CostName &&
                   PercentNumber == percent.PercentNumber;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine( CostName, PercentNumber);
        }
    }
}
