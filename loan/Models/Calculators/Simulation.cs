﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models.Calculators
{
    public class Simulation : Entity<long>
    {
        public virtual long PropertyPrices { set; get; }
        public virtual long PTKP { set; get; }
        public virtual long DownPayment { set; get; }
        public virtual decimal LoanInterestCost { set; get; }
        public virtual decimal HomeInsuranceCost { set; get; }
        public virtual long LifeInsuranceCost { set; get; }
        public virtual decimal AdministrativeCost { set; get; }
        public virtual decimal BPHTB { set; get; }
        public virtual decimal NotaryFee { set; get; }
        public virtual long PNPB { set; get; }
        public virtual long StampDuty { set; get; }

        public override bool Equals(object obj)
        {
            var simulation = obj as Simulation;
            return simulation != null &&
                   PropertyPrices == simulation.PropertyPrices &&
                   PTKP == simulation.PTKP &&
                   DownPayment == simulation.DownPayment &&
                   LoanInterestCost == simulation.LoanInterestCost &&
                   HomeInsuranceCost == simulation.HomeInsuranceCost &&
                   LifeInsuranceCost == simulation.LifeInsuranceCost &&
                   AdministrativeCost == simulation.AdministrativeCost &&
                   BPHTB == simulation.BPHTB &&
                   NotaryFee == simulation.NotaryFee &&
                   PNPB == simulation.PNPB &&
                   StampDuty == simulation.StampDuty;
        }

        public override int GetHashCode()
        {
            var hash = new HashCode();
            hash.Add(PropertyPrices);
            hash.Add(PTKP);
            hash.Add(DownPayment);
            hash.Add(LoanInterestCost);
            hash.Add(HomeInsuranceCost);
            hash.Add(LifeInsuranceCost);
            hash.Add(AdministrativeCost);
            hash.Add(BPHTB);
            hash.Add(NotaryFee);
            hash.Add(PNPB);
            hash.Add(StampDuty);
            return hash.ToHashCode();
        }
    }
}
