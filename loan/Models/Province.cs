﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models
{
    public class Province : Entity<long>
    {
        public virtual string ProvinceName { set; get; }
        public virtual IList<City> Cities { set; get; }

        public override bool Equals(object obj)
        {
            var province = obj as Province;
            return province != null &&
                   ProvinceName == province.ProvinceName &&
                   EqualityComparer<IList<City>>.Default.Equals(Cities, province.Cities);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine( ProvinceName, Cities);
        }
    }
}
