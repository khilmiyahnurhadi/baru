﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models
{
    public class PostalCode : Entity<long>
    {
        public virtual string PostalCodeNumber { set; get; }
        public virtual SubDistrict SubDistrictId { set; get; }
        public virtual IList<Village> Villages { set; get; }

        public override bool Equals(object obj)
        {
            var code = obj as PostalCode;
            return code != null &&
                   PostalCodeNumber == code.PostalCodeNumber &&
                   EqualityComparer<SubDistrict>.Default.Equals(SubDistrictId, code.SubDistrictId) &&
                   EqualityComparer<IList<Village>>.Default.Equals(Villages, code.Villages);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine( PostalCodeNumber, SubDistrictId, Villages);
        }
    }
}
