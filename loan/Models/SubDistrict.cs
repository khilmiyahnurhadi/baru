﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models
{
    public class SubDistrict : Entity<long>
    {
        public virtual string SubDistrictName { set; get; }
        public virtual City CityId { get; set; }
        public virtual IList<PostalCode> PostalCodes { set; get; }

        public override bool Equals(object obj)
        {
            var district = obj as SubDistrict;
            return district != null &&
                   SubDistrictName == district.SubDistrictName &&
                   EqualityComparer<City>.Default.Equals(CityId, district.CityId) &&
                   EqualityComparer<IList<PostalCode>>.Default.Equals(PostalCodes, district.PostalCodes);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine( SubDistrictName, CityId, PostalCodes);
        }
    }
}
