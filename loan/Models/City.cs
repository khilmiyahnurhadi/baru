﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models
{
    public class City : Entity<long>
    {
        public virtual string CityName { set; get; }
        public virtual Province ProvinceId { get; set; }
        public virtual IList<SubDistrict> SubDistricts { set; get; }

        public override bool Equals(object obj)
        {
            var city = obj as City;
            return city != null &&
                   CityName == city.CityName &&
                   EqualityComparer<Province>.Default.Equals(ProvinceId, city.ProvinceId) &&
                   EqualityComparer<IList<SubDistrict>>.Default.Equals(SubDistricts, city.SubDistricts);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine( CityName, ProvinceId, SubDistricts);
        }
    }
}
