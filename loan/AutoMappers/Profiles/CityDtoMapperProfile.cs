﻿using AutoMapper;
using loan.Dtos;
using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.AutoMappers.Profiles
{
    public class CityDtoMapperProfile : Profile
    {
        public CityDtoMapperProfile()
        {
            CreateMap<City, CityDto>()
                .ForMember(x => x.ProvinceId, opt => opt.MapFrom(src => src.ProvinceId))
                .ReverseMap();

        }
    }
}
