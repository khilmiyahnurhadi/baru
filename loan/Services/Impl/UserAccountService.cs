﻿using AutoMapper;
using loan.Commands;
using loan.Dtos.User;
using loan.Repositories;
using loan.WorkFlows;
using QSI.Common.Exception.Data.General;
using QSI.Common.Helper;
using QSI.Persistence.Query;
using QSI.Persistence.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Services.Impl
{ 
    public class UserAccountService : IUserAccountService
    {
        private readonly IMapper mapper;
        private readonly IUserBaseDao userBaseDao;
        private readonly ConditionFactory conditionFactory;
        public IdGenerator userAccIdGenerator { private get; set; }
        public IUserAccountRegistrationWorkflow workflow { private get; set; }

        public UserAccountService(IMapper mapper, IUserBaseDao userBaseDao, ConditionFactory conditionFactory)
        {
            this.mapper = mapper;
            this.userBaseDao = userBaseDao;
            this.conditionFactory = conditionFactory;
        }

        public string RegisterNewUserAccount(RegisterUserAccountCommand command)
        {
            Models.User.UserBase uac = mapper.Map<Models.User.UserBase>(command);
            uac.Username = userAccIdGenerator.Generate(uac);
            uac.FullName = command.FullName; 
            uac.Email = command.Email;
            uac.PhoneNumber = command.PhoneNumber;

            userBaseDao.Save(uac);

            return uac.Username;
        }

        [Transactional]
        public async Task<UserAccountRegistrationHintDto> RegisterUserAccount(string userAccountName, string username = null)
        {
            ICondition condition = conditionFactory.Create();
            condition.Column("Username").Equal(userAccountName);
            Models.User.UserBase userBase = userBaseDao.Get(condition);
            if (userBase == null)
                throw new InvalidDataVerificationException($"User with username {userAccountName} is not valid");

            var response = await workflow.RegisterUserAccount(userBase, username);

            return new UserAccountRegistrationHintDto
            {
                Username = response.Username,
                Email = "a@a.com",
                Otp = response.Otp
            };
        }
    }
}
