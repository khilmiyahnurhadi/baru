﻿using AutoMapper;
using loan.Commands;
using loan.Dtos;
using loan.Models;
using loan.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Services.Impl
{
    public class LocationService : ILocationService
    {
        
        protected readonly IProvinceDao provinceDao;
        protected readonly ICityDao cityDao;
        protected readonly ISubDistrictDao subDistrictDao;
        protected readonly IPostalCodeDao postalCodeDao;
        protected readonly IVillageDao villageDao;
        protected readonly IMapper mapper;

        public LocationService( IProvinceDao provinceDao, ICityDao cityDao, ISubDistrictDao subDistrictDao, IPostalCodeDao postalCodeDao, IVillageDao villageDao, IMapper mapper)
        { 
            this.provinceDao = provinceDao;
            this.cityDao = cityDao;
            this.subDistrictDao = subDistrictDao;
            this.postalCodeDao = postalCodeDao;
            this.villageDao = villageDao;
            this.mapper = mapper;
        }

        public void AddNewProvince(AddProvinceCommand command)
        {
            Province NewProvince = new Province();
            NewProvince.ProvinceName = command.ProvinceName;
            this.provinceDao.Save(NewProvince);
        }

        public void AddNewCity(AddCityCommand command)
        {
            City NewCity = new City();
            NewCity.CityName = command.CityName;
            var ProvinceId = this.provinceDao.Get(command.ProvinceId);
            NewCity.ProvinceId = ProvinceId;
            this.cityDao.Save(NewCity);
        }

        public void AddNewSubDistrict(AddSubDistrictCommand command)
        {
            SubDistrict NewSubDistrict = new SubDistrict();
            NewSubDistrict.SubDistrictName = command.SubDistrictName;
            var CityId = this.cityDao.Get(command.CityId);
            NewSubDistrict.CityId = CityId;
            this.subDistrictDao.Save(NewSubDistrict);
        }

        public void AddNewPostalCode(AddPostalCodeCommand command)
        {
            PostalCode NewPostalCode = new PostalCode();
            NewPostalCode.PostalCodeNumber = command.PostalCodeNumber;
            var SubDistrictId = this.subDistrictDao.Get(command.SubDistrictId);
            NewPostalCode.SubDistrictId = SubDistrictId;
            this.postalCodeDao.Save(NewPostalCode);
        }

        public void AddNewVillage(AddVillageCommand command)
        {
            Village NewVillage = new Village();
            NewVillage.VillageName = command.VillageName;
            var PostalCodeId = this.postalCodeDao.Get(command.PostalCodeId);
            NewVillage.PostalCodeId = PostalCodeId;
            this.villageDao.Save(NewVillage);
        }

        public void EditProvince(EditProvinceCommand command)
        {
            var Province = this.provinceDao.Get(command.IdProvince);
            Province.ProvinceName = command.ProvinceName;
            this.provinceDao.Save(Province);
        }
        public void EditCity(EditCityCommand command)
        {
            var City = this.cityDao.Get(command.IdCity);
            City.CityName = command.CityName;
            var ProvinceId = this.provinceDao.Get(command.ProvinceId);
            City.ProvinceId = ProvinceId;
            this.cityDao.Save(City);
        }
        public void EditSubDistrict(EditSubDistrictCommand command)
        {
            var SubDistrict = this.subDistrictDao.Get(command.IdSubDistrict);
            SubDistrict.SubDistrictName= command.SubDistrictName;
            var CityId = this.cityDao.Get(command.CityId);
            SubDistrict.CityId = CityId;
            this.subDistrictDao.Save(SubDistrict);
        }
        public void EditPostalCode(EditPostalCodeCommand command)
        {
            var PostalCode = this.postalCodeDao.Get(command.IdPostalCode);
            PostalCode.PostalCodeNumber= command.PostalCodeNumber;
            var SubDistrictId= this.subDistrictDao.Get(command.SubDistrictId);
            PostalCode.SubDistrictId= SubDistrictId;
            this.postalCodeDao.Save(PostalCode);
        }

        public void EditVillage(EditVillageCommand command)
        {
            var Village = this.villageDao.Get(command.IdVillage);
            Village.VillageName = command.VillageName;
            var PostalCodeId = this.postalCodeDao.Get(command.PostalCodeId);
            Village.PostalCodeId = PostalCodeId;
            this.villageDao.Save(Village);
        }

        public IList<ProvinceDto> GetAllProvince()
        {
            var provinces = this.provinceDao.GetAll();
            IList<ProvinceDto> provinceDtos = new List<ProvinceDto>();
            foreach (var province in provinces)
            {
                provinceDtos.Add(new ProvinceDto(province.Id, province.ProvinceName));
            }

            return provinceDtos;
        }
        public IList<CityDto> GetAllCity()
        {
            var cities = this.cityDao.GetAll("ProvinceId");
            
            IList<CityDto> cityDtos = new List<CityDto>();
            foreach (var city in cities)
            {
                this.mapper.Map<CityDto>(city);
                //var a = this.cityDao.Get(city.ProvinceId);
                //long a = new long();
                //City b = new City();
                // Hibernate.initialize(city.ProvinceId.getElement());
                //City p = ISessionFactory.getCurrentSession().load(city, city.ProvinceId);
                //long b = city.ProvinceId.ToString();
                
                Int64.TryParse(city.ProvinceId.ToString(), out long a);
                //var province = this.provinceDao.Get(a);
                cityDtos.Add(new CityDto(city.Id, city.CityName, a));
            }

            return cityDtos;
        }
        public IList<SubDistrictDto> GetAllSubDistrict()
        {
            var SubDistricts = this.subDistrictDao.GetAll();
            IList<SubDistrictDto> subDistrictDtos = new List<SubDistrictDto>();
            foreach (var subDistrict in SubDistricts)
            {
                Int64.TryParse(subDistrict.CityId.ToString(), out long a);
                subDistrictDtos.Add(new SubDistrictDto(subDistrict.Id, subDistrict.SubDistrictName, a));
            }

            return subDistrictDtos;
        }
        public IList<PostalCodeDto> GetAllPostalCode()
        {
            var PostalCodes = this.postalCodeDao.GetAll();
            IList<PostalCodeDto> postalCodeDtos = new List<PostalCodeDto>();
            foreach (var postalCode in PostalCodes)
            {
                postalCodeDtos.Add(new PostalCodeDto(postalCode.Id, postalCode.PostalCodeNumber));
            }

            return postalCodeDtos;
        }
        public IList<VillageDto> GetAllVillage()
        {
            var Villages = this.villageDao.GetAll();
            IList<VillageDto> villageDtos = new List<VillageDto>();
            foreach (var village in Villages)
            {
                villageDtos.Add(new VillageDto(village.Id, village.VillageName));
            }

            return villageDtos;
        }
    }
}
