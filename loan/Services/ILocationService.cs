﻿using loan.Commands;
using loan.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Services
{
    public interface ILocationService
    {
        void AddNewProvince(AddProvinceCommand command);
        void AddNewCity(AddCityCommand command);
        void AddNewSubDistrict(AddSubDistrictCommand command);
        void AddNewPostalCode(AddPostalCodeCommand command);
        void AddNewVillage(AddVillageCommand command);
        void EditProvince(EditProvinceCommand command);
        void EditCity(EditCityCommand command);
        void EditSubDistrict(EditSubDistrictCommand command);
        void EditPostalCode(EditPostalCodeCommand command);
        void EditVillage(EditVillageCommand command);
        IList<ProvinceDto> GetAllProvince();
        IList<CityDto> GetAllCity();
        IList<SubDistrictDto> GetAllSubDistrict();
        IList<PostalCodeDto> GetAllPostalCode();
        IList<VillageDto> GetAllVillage();
    }
}
