﻿using loan.Commands;
using loan.Dtos.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Services
{
    public interface IUserAccountService
    {
        string RegisterNewUserAccount(RegisterUserAccountCommand command);
        Task<UserAccountRegistrationHintDto> RegisterUserAccount(string userAccountName, string username = null);
    }
}
