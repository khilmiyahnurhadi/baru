﻿using loan.Commands;
using loan.Dtos.Calculators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Services
{
    public interface ICalculatorService
    {
        IList<PTKPDto> GetAllPTKP();
        SimulationDto SimulationResult(SimulationCommand command);
        IList<SimulationDto> GetAllSimulation();
        void EditHomeInsuranceCost(EditHomeInsuranceCommand command);
        void EditLifeInsuranceCost(EditLifeInsuranceCommand command);
    }
}
